package com.commonsware.android.download

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import androidx.recyclerview.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val PERMS_READ = 1337
private const val PERMS_WRITE = 1338
private const val URL = "https://commonsware.com/presos/2019-08-RxWorkshop.pdf"
private const val FILENAME = "2019-08-RxWorkshop.pdf"
private const val MIME_TYPE = "application/pdf"

class MainActivity : AppCompatActivity() {
  private val motor: MainMotor by viewModel()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    LinearLayoutManager(this).let {
      titles.layoutManager = it
      titles.addItemDecoration(DividerItemDecoration(this, it.orientation))
    }

    val adapter = TitleAdapter(layoutInflater)

    titles.adapter = adapter

    motor.states.observe(this) { state ->
      when (state) {
        MainViewState.Loading -> {
          loading.visibility = View.VISIBLE
          adapter.submitList(listOf())
        }
        is MainViewState.Content -> {
          loading.visibility = View.GONE
          adapter.submitList(state.titles)
        }
        MainViewState.Error -> {
          loading.visibility = View.GONE
          adapter.submitList(listOf())
          Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show()
        }
      }
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
      motor.refresh()
    }
    else {
      loadAll()
    }
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.actions, menu)

    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.refresh -> motor.refresh()
      R.id.download -> download()
      R.id.all -> loadAll()
      else -> return super.onOptionsItemSelected(item)
    }

    return true
  }

  override fun onRequestPermissionsResult(
    requestCode: Int,
    permissions: Array<out String>,
    grantResults: IntArray
  ) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    if (requestCode == PERMS_READ) {
      if (grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED) {
        motor.refresh()
      }
    } else if (requestCode == PERMS_WRITE) {
      if (grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED) {
        motor.download(URL, FILENAME, MIME_TYPE)
      }
    }
  }

  private fun loadAll() {
    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
      motor.refresh()
    } else {
      requestPermissions(
        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
        PERMS_READ
      )
    }
  }

  private fun download() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q ||
      checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
      motor.download(URL, FILENAME, MIME_TYPE)
    } else {
      requestPermissions(
        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
        PERMS_WRITE
      )
    }
  }
}

class TitleAdapter(private val inflater: LayoutInflater) :
  ListAdapter<String, RowHolder>(StringDiffer) {
  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ) = RowHolder(
    inflater.inflate(
      android.R.layout.simple_list_item_1,
      parent,
      false
    )
  )

  override fun onBindViewHolder(holder: RowHolder, position: Int) {
    holder.bind(getItem(position))
  }
}

class RowHolder(root: View) : RecyclerView.ViewHolder(root) {
  private val title = root.findViewById<TextView>(android.R.id.text1)

  fun bind(text: String) {
    title.text = text
  }
}

private object StringDiffer : DiffUtil.ItemCallback<String>() {
  override fun areItemsTheSame(oldItem: String, newItem: String) =
    oldItem === newItem

  override fun areContentsTheSame(oldItem: String, newItem: String) =
    oldItem == newItem
}
